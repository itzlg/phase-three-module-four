/*
Navicat MySQL Data Transfer

Source Server         : localmysql
Source Server Version : 50537
Source Host           : 127.0.0.1:3306
Source Database       : javalearn

Target Server Type    : MYSQL
Target Server Version : 50537
File Encoding         : 65001

Date: 2020-08-03 02:38:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lagou_auth_code
-- ----------------------------
DROP TABLE IF EXISTS `lagou_auth_code`;
CREATE TABLE `lagou_auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱地址',
  `code` varchar(6) DEFAULT NULL COMMENT '验证码',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `expiretime` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
