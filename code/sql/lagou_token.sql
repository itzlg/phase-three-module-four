/*
Navicat MySQL Data Transfer

Source Server         : localmysql
Source Server Version : 50537
Source Host           : 127.0.0.1:3306
Source Database       : javalearn

Target Server Type    : MYSQL
Target Server Version : 50537
File Encoding         : 65001

Date: 2020-08-03 02:38:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lagou_token
-- ----------------------------
DROP TABLE IF EXISTS `lagou_token`;
CREATE TABLE `lagou_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `email` varchar(64) NOT NULL COMMENT '邮箱地址',
  `token` varchar(255) NOT NULL COMMENT '令牌',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
