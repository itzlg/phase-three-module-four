$(function() {

    $("#btn_getCode").on('click',function () {

    	var email = $("#email").val();
    	// 异步请求,经过网关路由给验证码微服务,生成验证码,并调用邮件微服务发送验证码邮件
    	$.ajax({
    		type: 'GET',
    		url: '/api/code/create/'+email,
    		contentType: 'application/json;charset=utf-8',
    		dataType: 'json',
    		/*xhrFields: {
				withCredentials: true // 设置运行跨域操作
			},*/
    		success: function(data) {
    			console.log(data);
    			if (data == true) {
    				$('#modal-success').modal('show');
    				$('#modal-success-hint').text("验证码已成功发送到您的注册邮箱!");
    			}
    		}
    	});

    	setTime(this);
    });

    function setTime(obj) {
		// 控制一分钟内只允许获取一次验证码
    	var time = 60;
        $(this).attr("disabled",true);
        var timer = setInterval(function () {
            if(time == 0){
                $("#btn_getCode").removeAttr("disabled");
                $("#btn_getCode").html("重新发送");
                clearInterval(timer);
            } else {
                $("#btn_getCode").html(time);
                time--;
            }
        },1000);
    }

	$("#btn_register").bind("click",function () {

		//省略前端数据标准格式校验部分......
		
		var email = $("#email").val();
		if(email == ""){
			$('#modal-fail').modal('show');
			$('#modal-fail-hint').text("邮箱不能为空!");
		}
		var password = $("#password").val();
		if(password == ""){
			$('#modal-fail').modal('show');
			$('#modal-fail-hint').text("密码不能为空!");
		}
		
		var confirmPassword = $("#confirmPassword").val();
		if(password != confirmPassword){
			$('#modal-fail').modal('show');
			$('#modal-fail-hint').text("两次密码不一致!");
		}
		var code = $("#code").val();
		if(code == ""){
			$('#modal-fail').modal('show');
			$('#modal-fail-hint').text("验证码不能为空!");
		}
        // 发送ajax请求
        $.ajax({
        	type: 'POST',
            url: '/api/user/register/'+ email + "/" + password + "/" + code,
            contentType: 'application/json;charset=utf-8',
            dataType: 'json',
            success: function (data) {
				console.log(data);
                if(data == true){
                	$('#modal-success').modal('show');
    				$('#modal-success-hint').text("注册成功，3秒后自动跳转欢迎页!");
    				setTimeout(function () {
	                    $.ajax({
	                    	type: 'POST',
	                        url: "/api/user/login/" + email + "/" + password,
	                        async: true,
	                        success: function (data) {
	                            window.location.href = "./welcome.html?email=" + data;
	                        },
	                        error: function (data) {
	                            alert(data.responseText);
	                        }
	                    });
	                },3000);
				}else{
					$('#modal-fail').modal('show');
					$('#modal-fail-hint').text("注册失败!");
				}
            },
			statusCode: {
				303: function(data) {
					$('#modal-fail').modal('show');
					$('#modal-fail-hint').text(data.responseText);
				},
				509: function(data) {
					$('#modal-fail').modal('show');
					$('#modal-fail-hint').text(data.responseText);
				}
			}
        })

    })

})