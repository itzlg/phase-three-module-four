$(function() {

	$("#btn_login").bind("click",function () {	
		var email = $("#email").val();
		if(email == ""){
			$('#modal-fail').modal('show');
			$('#modal-fail-hint').text("邮箱不能为空!");
		}
		var password = $("#password").val();
		if(password == ""){
			$('#modal-fail').modal('show');
			$('#modal-fail-hint').text("密码不能为空!");
		}		

	    // 发送ajax请求
	    $.ajax({
	    	type: 'POST',
	        url: '/api/user/login/'+ email + "/" + password,
	        contentType: 'application/json;charset=utf-8',
	        dataType: 'json',
	        /*success: function (data) {
				console.log(data);
	            if(data == email){
					alert("登录成功");
					location.href = "./welcome.html";
				}else{
					alert(data);
				}
	        },*/
			statusCode: {
				200: function(data) {
				    data = data.responseText;
					console.log(data);
					if(data == email){
						$('#modal-success').modal('show');
    					$('#modal-success-hint').text("登录成功!");
						location.href = "./welcome.html?email="+data;
					}else{
						$('#modal-fail').modal('show');
						$('#modal-fail-hint').text(data);
					}
				},
				401: function(data) {
					$('#modal-fail').modal('show');
					$('#modal-fail-hint').text(data.responseText);
				},
				303: function(data) {
					$('#modal-fail').modal('show');
					$('#modal-fail-hint').text(data.responseText);
				}
			}
	    })

	})



})