package com.fishleap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zlg
 */
@SpringBootApplication
@EnableDiscoveryClient
@EntityScan("com.fishleap.pojo")
public class LagouEmailApp8082 {

    public static void main(String[] args) {
        SpringApplication.run(LagouEmailApp8082.class, args);
    }
}
