package com.fishleap.controller;

import com.fishleap.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author zlg
 * @create 2020-08-01 1:19
 */
@RestController
@RequestMapping("/api/email")
public class EmailController {

    @Autowired
    private EmailService emailService;

    /**
     * 发送验证码邮件
     * @param email 邮箱
     * @param code 验证码
     * @return 发送结果
     */
    @GetMapping("/{email}/{code}")
    public Boolean sendEmail(@PathVariable("email")String email, @PathVariable("code")String code) {
        Boolean result = emailService.sendMail(email, "注册验证码", code);
        if (result) {
            System.out.println("验证码发送成功!");
        }
        return result;
    }


}
