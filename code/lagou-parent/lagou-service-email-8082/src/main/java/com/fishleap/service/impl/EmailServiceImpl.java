package com.fishleap.service.impl;

import com.fishleap.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;

import javax.mail.internet.MimeMessage;

/**
 * @author zlg
 */
@Service
public class EmailServiceImpl implements EmailService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JavaMailSender mailSender;

    //邮件发件人
    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    TemplateEngine templateEngine;

    @Override
    public Boolean sendMail(String to, String subject, String verifyCode) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setSubject(subject);
            helper.setTo(to);
            helper.setText(verifyCode, true);
            mailSender.send(message);
            //日志信息
            logger.info("邮件已经发送。");
            return true;
        } catch (Exception e) {
            logger.error("发送邮件时发生异常！", e);
            return false;
        }
    }

}
