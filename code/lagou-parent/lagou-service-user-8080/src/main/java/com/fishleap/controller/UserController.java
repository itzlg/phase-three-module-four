package com.fishleap.controller;

import com.fishleap.client.CodeServiceFeignClient;
import com.fishleap.pojo.Token;
import com.fishleap.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * @author zlg
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private CodeServiceFeignClient codeServiceFeignClient;

    /**
     * 注册接口，true成功，false失败,生成token并写入cookie中
     * @param email 邮箱
     * @return true/false
     */
    @PostMapping("/register/{email}/{password}/{code}")
    public Boolean register(@PathVariable("email")String email,@PathVariable("code")String code,
                            HttpServletResponse response) {
        //判断邮箱是否已注册
        if(isRegistered(email)) {
            return false;
        }
        //判断code是否正确
        if(codeServiceFeignClient.checkCode(email,code) != 0) {
            return false;
        }
        //持久化到数据库
        Token token = new Token();
        token.setEmail(email);
        token.setToken(UUID.randomUUID().toString());
        tokenService.addUser(token);
        //将token写入cookie中
        Cookie cookie = new Cookie("token", token.getToken());
        cookie.setMaxAge(30*60);
        cookie.setPath("/");
        response.addCookie(cookie);
        return true;
    }

    /**
     * 是否已注册，根据邮箱判断,true代表已经注册过，false代表尚未注册
     * @param email 邮箱
     * @return true/false
     */
    @GetMapping("/isRegistered/{email}")
    public Boolean isRegistered(@PathVariable("email")String email) {
        return tokenService.findEmail(email);
    }


    /**
     * 登录接口，验证用户名密码合法性，根据用户名和密码生成token，token存入数据库，并写入cookie
     * 中，登录成功返回邮箱地址，重定向到欢迎页
     * @param email 邮箱
     * @param password 密码
     * @return 邮箱
     */
    @PostMapping("/login/{email}/{password}")
    public String login(@PathVariable("email")String email, @PathVariable("password")String password,
                        HttpServletResponse response) {
        //判断邮箱是否已注册
        if(!isRegistered(email)) {
            return "邮箱没有注册！";
        }
        //查询Token
        Token token = tokenService.getToken(email);
        //生成token并写入数据库
        token.setToken(UUID.randomUUID().toString());
        tokenService.addUser(token);
        // 将token添加到cookie中
        Cookie cookie = new Cookie("token", token.getToken());
        cookie.setMaxAge(30*60);
        cookie.setPath("/");
        response.addCookie(cookie);
        return email;
    }


    /**
     * 根据token查询用户登录邮箱接口
     * @param token 令牌
     * @return 邮箱
     */
    @GetMapping("/info/{token}")
    public String getEmail(@PathVariable("token")String token) {
        return tokenService.findEmailByToken(token);
    }

}
