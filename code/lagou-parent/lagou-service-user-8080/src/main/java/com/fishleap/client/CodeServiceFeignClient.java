package com.fishleap.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zlg
 */
@FeignClient(name = "lagou-service-code")
@RequestMapping("/api/code")
public interface CodeServiceFeignClient {

    @GetMapping("/validate/{email}/{code}")
    public Integer checkCode(@PathVariable("email") String email, @PathVariable("code") String code);

}