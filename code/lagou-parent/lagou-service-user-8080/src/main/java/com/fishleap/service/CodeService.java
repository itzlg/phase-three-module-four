package com.fishleap.service;

import com.fishleap.pojo.AuthCode;

/**
 * @author zlg
 */
public interface CodeService {
    AuthCode saveAuthCode(String email, String code);

    Integer checkCode(String email, String code);

}
