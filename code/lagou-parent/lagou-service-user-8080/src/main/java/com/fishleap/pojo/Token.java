package com.fishleap.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @author zlg
 */
@Data
@Entity
@Table(name = "lagou_token")
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String email;
    private String token;
}
