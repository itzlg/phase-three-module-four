package com.fishleap.service.impl;

import com.fishleap.dao.CodeDao;
import com.fishleap.pojo.AuthCode;
import com.fishleap.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author zlg
 */
@Service
public class CodeServiceImpl implements CodeService {

    @Autowired
    private CodeDao codeDao;


    @Override
    public AuthCode saveAuthCode(String email, String code) {
        // 验证码10分钟内有效
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expire = now.plusMinutes(10);
        String createtime = formatter.format(now);
        String expiretime = formatter.format(expire);
        // 持久化到数据库
        AuthCode authCode = new AuthCode();
        authCode.setEmail(email);
        authCode.setCode(code);
        authCode.setCreatetime(createtime);
        authCode.setExpiretime(expiretime);

        AuthCode authCodeData = codeDao.save(authCode);
        return authCodeData;
    }

    @Override
    public Integer checkCode(String email, String code) {
        AuthCode ac = new AuthCode();
        ac.setEmail(email);
        Example<AuthCode> example = Example.of(ac);
        AuthCode authCode = codeDao.findOne(example).get();
        if (code.equals(authCode.getCode())) {
            String now = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss").format(LocalDateTime.now());
            if (now.compareTo(authCode.getExpiretime()) != 1) {
                return 0;   // 验证码正确
            }
            return 2;   // 验证码超时
        }else {
            return 1;   // 验证码错误
        }
    }


}
