package com.fishleap.service.impl;

import com.fishleap.dao.TokenDao;
import com.fishleap.pojo.Token;
import com.fishleap.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zlg
 */
@Service
@Transactional
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenDao tokenDao;

    @Override
    public Boolean findEmail(String email) {
        Token token = new Token();
        token.setEmail(email);
        Example<Token> example = Example.of(token);
        return tokenDao.findOne(example).isPresent();
    }

    @Override
    public void addUser(Token token) {
        Token save = tokenDao.save(token);
    }

    @Override
    public void addToken(String email, String token) {
        Token t = new Token();
        t.setEmail(email);
        t.setToken(token);
        tokenDao.save(t);
    }

    @Override
    public String findEmailByToken(String token) {
        Token t = new Token();
        t.setToken(token);
        Example<Token> example = Example.of(t);
        Token result = tokenDao.findOne(example).get();
        if (result != null) {
            return result.getEmail();
        }
        return "该token没有查询到用户邮箱！";
    }

    @Override
    public Token getToken(String email) {
        Token token = new Token();
        token.setEmail(email);
        Example<Token> example = Example.of(token);
        return tokenDao.findOne(example).get();
    }

}
