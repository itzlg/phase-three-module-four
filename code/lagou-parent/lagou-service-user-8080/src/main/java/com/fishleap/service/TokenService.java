package com.fishleap.service;

import com.fishleap.pojo.Token;

/**
 * @author zlg
 */
public interface TokenService {
    Boolean findEmail(String email);

    void addUser(Token token);

    void addToken(String email, String token);

    String findEmailByToken(String token);

    Token getToken(String email);
}
