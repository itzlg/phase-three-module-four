package com.fishleap.dao;

import com.fishleap.pojo.Token;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zlg
 */
public interface TokenDao extends JpaRepository<Token, Integer> {
}
