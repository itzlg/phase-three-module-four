package com.fishleap.dao;

import com.fishleap.pojo.AuthCode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zlg
 */
public interface CodeDao extends JpaRepository<AuthCode, Integer> {
}
