package com.fishleap.client;

import org.springframework.stereotype.Component;

/**
 * @author zlg
 * 降级回退逻辑需要定义一个类，实现FeignClient接口，实现接口中的方法
 */
@Component
public class EmailFallBack implements EmailServiceFeignClient {
    @Override
    public Boolean sendEmail(String email, String code) {
        // 当邮件微服务不可用或超时时,服务降价默认返回false,发送邮件失败
        return false;
    }
}
