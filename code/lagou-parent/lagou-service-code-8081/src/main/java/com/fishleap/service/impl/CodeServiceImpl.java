package com.fishleap.service.impl;

import com.fishleap.dao.CodeDao;
import com.fishleap.pojo.AuthCode;
import com.fishleap.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * @author zlg
 */
@Service
public class CodeServiceImpl implements CodeService {

    @Autowired
    private CodeDao codeDao;

    @Override
    public AuthCode saveAuthCode(String email, String code) {
        // 验证码10分钟内有效
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expire = now.plusMinutes(10);
        String createtime = formatter.format(now);
        String expiretime = formatter.format(expire);
        // 查找是否存在该邮箱
        AuthCode authCode = new AuthCode();
        authCode.setEmail(email);
        Example<AuthCode> example = Example.of(authCode);
        Optional<AuthCode> one = codeDao.findOne(example);
        // 持久化到数据库
        authCode.setCode(code);
        authCode.setCreatetime(createtime);
        authCode.setExpiretime(expiretime);
        if (one.isPresent()) {
            authCode.setId(one.get().getId());    // 存在id时，根据id修改数据
        }
        return codeDao.save(authCode);
    }

    @Override
    public Integer checkCode(String email, String code) {
        AuthCode ac = new AuthCode();
        ac.setEmail(email);
        Example<AuthCode> example = Example.of(ac);
        AuthCode authCode = codeDao.findOne(example).get();
        if (code.equals(authCode.getCode())) {
            String now = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss").format(LocalDateTime.now());
            if (now.compareTo(authCode.getExpiretime()) != 1) {
                return 0;   // 验证码正确
            }
            return 2;   // 验证码超时
        }else {
            return 1;   // 验证码错误
        }
    }
}
