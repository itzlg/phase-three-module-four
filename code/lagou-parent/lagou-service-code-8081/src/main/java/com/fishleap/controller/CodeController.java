package com.fishleap.controller;

import com.fishleap.client.EmailServiceFeignClient;
import com.fishleap.pojo.AuthCode;
import com.fishleap.service.CodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @author zlg
 */
@RestController
@RequestMapping("/api/code")
public class CodeController {

    private static final Logger logger = LoggerFactory.getLogger(CodeController.class);

    // 引入邮件微服务实例进行远程方法调用
    @Autowired
    private EmailServiceFeignClient emailServiceFeignClient;
    @Autowired
    private CodeService codeService;


    /**
     * 给指定的邮件地址发送执行内容的邮件
     * @param email 邮箱
     * @return true/false
     */
    @GetMapping("/create/{email}")
    public Boolean createCode(@PathVariable("email")String email) {
        logger.info("REST request to create code and send {}", email);
        // 随机生成6位数的验证码
        Random random = new Random();
        String code = "";
        for (int i = 0; i < 6; i++) {
            code += random.nextInt(10);
        }
        logger.info("code ========>>> {}", code);
        // 持久化验证码认证信息
        AuthCode authCode = codeService.saveAuthCode(email, code);
        if (authCode == null) {
            logger.info("authcode insert into database success!");
            return false;
        }
        // 远程调用邮件微服务,发送验证码邮件
        String finalCode = code;
        Thread thread = new Thread(() -> {
            Boolean result = emailServiceFeignClient.sendEmail(email, finalCode);
        });
        thread.start();
        return true;
    }


    /**
     * 校验验证码是否正确，0正确1错误2超时
     * @param email 邮箱
     * @param code 验证码
     * @return 0/1/2
     */
    @GetMapping("/validate/{email}/{code}")
    public Integer checkCode(@PathVariable("email")String email,@PathVariable("code")String code) {
        logger.info("REST request to check code by {}", email);
        Integer result = codeService.checkCode(email, code);
        return result;
    }

}
