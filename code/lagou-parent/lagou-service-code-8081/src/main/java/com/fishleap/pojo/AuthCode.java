package com.fishleap.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @author zlg
 */
@Data
@Entity
@Table(name = "lagou_auth_code")
public class AuthCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String email;
    private String code;
    private String createtime;
    private String expiretime;
}
