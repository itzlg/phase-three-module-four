package com.fishleap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author zlg
 */
@SpringBootApplication
@EnableEurekaServer
public class LagouEurekaServerApp8761 {

    public static void main(String[] args) {
        SpringApplication.run(LagouEurekaServerApp8761.class, args);
    }
}
