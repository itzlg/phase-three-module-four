package com.fishleap.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author zlg
 */
@FeignClient(value = "lagou-service-user",path = "/api/user")
public interface UserServiceFeignClient {

    @GetMapping("/info/{token}")
    public String getEmail(@PathVariable("token")String token);

}
