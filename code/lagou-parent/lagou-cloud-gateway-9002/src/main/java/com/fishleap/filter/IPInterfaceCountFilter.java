package com.fishleap.filter;

import com.fishleap.util.IPUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author zlg
 * IP接口防爆刷过滤
 * 定义全局过滤器，会对所有路由生效
 */
@Slf4j
@Component  // 让容器扫描到，等同于注册了
@RefreshScope   // 修改config仓库的配置后发送请求手动刷新
public class IPInterfaceCountFilter implements GlobalFilter, Ordered, Runnable {

    // 存储同一ip在限制时间内的请求次数
    private static Map<String,Integer> ipCountMap = new ConcurrentHashMap<>();

    //单个在多少分钟内请求注册接口
    @Value("${gateway.limitMinutes}")
    private String limitMinutes;    // 1

    //不能超过的次数
    @Value("${gateway.limitCount}")
    private String limitCount;      // 1

    //创建线程定时任务,每隔1分钟执行一次任务,清空ip接口存储列表
    public IPInterfaceCountFilter () {
        Executors.newSingleThreadScheduledExecutor()
                .scheduleWithFixedDelay(this, 6, 1, TimeUnit.MINUTES);
    }

    /**
     * 过滤器核心方法
     * @param exchange 封装了request和response对象的上下文
     * @param chain 网关过滤器链（包含全局过滤器和单路由过滤器）
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println(limitMinutes+"====>>>"+limitCount);
        ServerHttpRequest request = exchange.getRequest();
//        String path = request.getPath().pathWithinApplication().value();
//        HttpMethod method = request.getMethod();
        // 获取路由的目标URI
//        URI targetUri = exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR);
//        /api/user/register/1321340821@qq.com/11/885444----POST--------null
//        System.out.println(path+"----"+method+"--------"+targetUri);


        // 从request对象中获取客户端ip
//        String clientIp = exchange.getRequest().getRemoteAddress().getHostName();
        // 获取真实请求的 IP 地址
        String clientIp = IPUtils.getIpAddress(request);
        // IPv6转成IPv4格式
        if (clientIp.equals("0:0:0:0:0:0:0:1")) {
            clientIp = "127.0.0.1";
        }
        System.out.println("ip==========>>>>>>"+clientIp);
        String path = exchange.getRequest().getPath().value();
        System.out.println("Path==========>>>>>"+exchange.getRequest().getPath().value());
        // 判断是否是注册接口
        if (path.indexOf("/register") >= 0) {
            if (ipCountMap.containsKey(clientIp)) {
                System.out.println("IPCountExist======>>>>" + ipCountMap.get(clientIp));
                if (ipCountMap.get(clientIp) >= Integer.parseInt(limitCount)) {
                    // 拒绝访问，返回
                    exchange.getResponse().setStatusCode(HttpStatus.SEE_OTHER); // 状态码 303
                    log.debug("=====>IP:" + clientIp + "当前时间段内注册次数过多!");
                    String data = "您频繁进行注册，请求已被拒绝!";
                    DataBuffer wrap = exchange.getResponse().bufferFactory().wrap(data.getBytes());
                    return exchange.getResponse().writeWith(Mono.just(wrap));
                } else {
                    ipCountMap.put(clientIp, ipCountMap.get(clientIp) + 1);
                }
            } else {
                ipCountMap.put(clientIp, 1);
                System.out.println("IPCount======>>>>" + ipCountMap.get(clientIp));
            }
        }
        // 合法请求，放行，执行后续的过滤器
        System.out.println("===============================================");
        return chain.filter(exchange);
    }


    /**
     * 返回值表示当前过滤器的顺序(优先级)，数值越小，优先级越高
     * @return 0
     */
    @Override
    public int getOrder() {
        return 0;
    }

    // 每隔1分钟清空一次ip接口请求次数的集合
    @Override
    public void run() {
        ipCountMap.clear();
    }
}
