package com.fishleap.filter;

import com.fishleap.client.UserServiceFeignClient;
import org.apache.commons.codec.Charsets;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author zlg
 * 用户进行token的验证
 * 用户微服务和验证码微服务的请求不不过滤（网关调用下游用户微服务的token验证接口）
 */
@Component
public class UserTokenFilter implements GlobalFilter, Ordered {

    @Autowired
    private UserServiceFeignClient userServiceFeignClient;

    /**
     * 过滤器核心方法
     * @param exchange 封装了request和response对象的上下文
     * @param chain 网关过滤器链（包含全局过滤器和单路由过滤器）
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 用户微服务和验证码微服务的请求不不过滤
        ServerHttpRequest request = exchange.getRequest();
        String path = request.getURI().getPath();
        if (path.contains("/api/user") || path.contains("/api/code")) {
            return chain.filter(exchange);
        }
        // 网关调用下游用户微服务的token验证接口
        List<HttpCookie> tokens = request.getCookies().get("token");
        if (CollectionUtils.isEmpty(tokens) || tokens.isEmpty()) {
            // 拒绝访问，返回
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED); // 状态码 401
            exchange.getResponse().getHeaders().set("Content-Type","application/json;charset=UTF-8");
            String data = "您的token无效!";
            DataBuffer wrap = exchange.getResponse().bufferFactory().wrap(data.getBytes(Charsets.UTF_8));
            return exchange.getResponse().writeWith(Mono.just(wrap));
        }
        // 查询是否有该token
        for (HttpCookie cookie : tokens) {
            String email = userServiceFeignClient.getEmail(cookie.getValue());
            if (StringUtils.isNotBlank(email)) {
                return chain.filter(exchange);
            }
        }

        // 拒绝访问，返回
        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED); // 状态码 401
        exchange.getResponse().getHeaders().set("Content-Type","application/json;charset=UTF-8");
        String data = "您的token无效!";
        DataBuffer wrap = exchange.getResponse().bufferFactory().wrap(data.getBytes(Charsets.UTF_8));
        return exchange.getResponse().writeWith(Mono.just(wrap));
    }


    /**
     * 返回值表示当前过滤器的顺序(优先级)，数值越小，优先级越高
     * @return 1
     */
    @Override
    public int getOrder() {
        return 1;
    }
}
